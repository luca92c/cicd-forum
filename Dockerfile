FROM php:7.4-fpm-alpine

# Install packages
#RUN apk update && php7-mbstring php7-bcmath php7-xml php7-sqlite3
RUN apk update && apk add --no-cache bash coreutils grep vim

# Install Composer
RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer


# Install app
RUN rm -rf /var/www/localhost/* && mkdir -p /var/www/html
ADD . /var/www/html

#Install Nginx
RUN apk --update add nginx && \
    mkdir -p /var/log/nginx && \
    touch /var/log/nginx/access.log && \
    mkdir -p /run/nginx


ADD nginx.conf /etc/nginx/
ADD php-fpm.conf /etc/php7.4/php-fpm.conf

EXPOSE 80
CMD php-fpm -d variables_order="EGPCS" && (tail -F /var/log/nginx/access.log &) && exec nginx -g "daemon off;"
